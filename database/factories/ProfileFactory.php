<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Profile;
use Faker\Generator as Faker;

$factory->define(Profile::class, function (Faker $faker) {
    return [
        'birthday' => $faker->date('Y-m-d', now()),
        'name' => $faker->name,
        'country' => $faker->country,
        'city' => $faker->city,
        'gender' => $faker->boolean,
        'status' => $faker->text(511),
        'about' => $faker->text(2000),
    ];
});
