<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Task;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
    return [
        'description' => $faker->text(511),
        'points' => $faker->numberBetween(5,10),
        'text' => $faker->text(2000),
    ];
});
