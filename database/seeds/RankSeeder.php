<?php

use App\Rank;
use Illuminate\Database\Seeder;

class RankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rank::create(['title' => 'Новичок', 'points' => 0]);
        Rank::create(['title' => 'Пионер', 'points' => 100]);
        Rank::create(['title' => 'Волонтер', 'points' => 200]);
        Rank::create(['title' => 'Старший волонтер', 'points' => 500]);
        Rank::create(['title' => 'Добродеятель', 'points' => 1000]);
    }
}
