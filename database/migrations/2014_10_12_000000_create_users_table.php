<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('email')->unique();
            $table->string('verified_key')->nullable();
            $table->string('password');
            $table->integer('points')->default(10);
            $table->integer('rating')->default(0);
            $table->boolean('is_verified')->default(false);
            $table->integer('profile_id')->nullable();
            $table->string('role', 255)->nullable();
            /*
             * null - common user
             * admin - admin
             * etc
            */
            $table->integer('status')->default(1);
            /*
             * 0 - ban
             * 1 - active
             * 2 - confirm
             * 3 - profile is fill
            */
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
