@extends('layouts.main')
@section('content')
    <div class="row">
        <div class="col-sm-12 col-lg-12 col-md-12 col-xl-12 text">
            <h1>Создание задачи</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Заполните все поля и ваша задача будет опубликована!</h3>
                        <p>Учтите, что дальнейшее редактирование невозможно, будьте предельно внимательны </p>
                    </div>
                    <div class="form-top-right">
                        <i class="fa fa-pencil"></i>
                    </div>
                </div>
                <div class="form-bottom">
                    <form role="form" action="{{route('tasks.store')}}" method="post"
                          class="registration-form">
                        @csrf
                        <div class="form-group">
                            <label for="form-points">
                                <strong><b>Сколько баллов вы готовы дать за выполнение?</b></strong>
                            </label>
                            <input type="number" class="form-control" placeholder="Не менее 5 баллов" name="points" id="form-points" required value="{{old('score')}}">
                        </div>
                        <div class="form-group">
                            <label for="form-description">
                                <strong><b>Краткое описание</b></strong>
                            </label>
                            <textarea name="description" maxlength="511" required class="form-control" placeholder="Постарайтесь уложиться в пару предложений"
                                      id="form-description">{{old('description')}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="form-text"><strong><b>Полное описание</b></strong></label>
                            <textarea name="text" placeholder="Расскажите о своей задаче максимально подробно, чтобы у волонтеров не оставалось вопросов"
                                      required maxlength="10000"
                                      class="form-textarea form-control" id="form-text" rows="10"
                                      cols="10">{{old('text')}}</textarea>
                        </div>
                        <button type="submit" class="btn">Создать</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
