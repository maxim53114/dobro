@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="row">
            @foreach($tasks as $task)
                <div class="col-md-6 col-lg-6 col-xl-4 col-sm-6">
                    <div class="card">
                        <img class="card-img-top" src="{{$task->author->profile->getImage()}}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">{{$task->author->profile->name}}<i class="fa fa-user" style="color:#5259ec; cursor: pointer" title="Это проверенный пользователь"></i></h5>
                            <p class="card-text">
                                {{$task->description}}
                            </p>
                            <div>За выполнение дам {{$task->points}} баллов</div>
                            <a href="{{route('tasks.show', ['id' => $task->id])}}" class="btn btn-primary">Помочь</a>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>

@endsection
