@extends('layouts.main')

@section('content')
    <div id="main">
        <div class="row" id="real-estates-detail">
            <div class="col-lg-4 col-md-4 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <header class="panel-title">
                            <div class="text-center">
                                <a href="{{route('profile.edit')}}"><strong>Редактировать профиль</strong></a>
                            </div>
                        </header>
                    </div>
                    <div class="panel-body">
                        <div class="text-center" id="author">
                            <img src="{{$user->profile->getImage()}}">
                            <h3>{{$user->profile->name}}</h3>
                            <small class="label label-warning">{{$user->profile->country}}</small>
                            <p>{{$user->profile->status}}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-xs-12">
                <div class="panel">
                    <div class="panel-body">
                        <h4>Профиль</h4>
                        <table class="table table-th-block">
                            <tbody>
                            <tr>
                                <td class="active">Зарегистрирован:</td>
                                <td>{{$user->created_at->format('d-m-Y')}}</td>
                            </tr>
                            <tr>
                                <td class="active">Страна:</td>
                                <td>{{$user->profile->country}}</td>
                            </tr>
                            <tr>
                                <td class="active">Город:</td>
                                <td>{{$user->profile->city}}</td>
                            </tr>
                            <tr>
                                <td class="active">Пол:</td>
                                <td>{{$user->profile->getGenderText()}}</td>
                            </tr>
                            <tr>
                                <td class="active">Полных лет:</td>
                                <td>{{$user->profile->getAge()}}</td>
                            </tr>
                            <tr>
                                <td class="active">Ранг:</td>
                                <td>{{$user->getRank()}}</td>
                            </tr>
                            <tr>
                                <td class="active">Число баллов:</td>
                                <td>{{$user->points}}</td>
                            </tr>
                            <tr>
                                <td class="active">Рейтинг: </td>
                                <td>{{$user->rating}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-12">
                <h3>Полная информация: </h3>
                {{ $user->profile->about }}
            </div>
        </div>
    </div>

@endsection
