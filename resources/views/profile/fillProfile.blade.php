@extends('layouts.main')
@section('content')
    <div class="row">
        <div class="col-sm-12 col-lg-12 col-md-12 col-xl-12 text">
            <h1>Заполните ваш профиль</h1>
            <div class="description">
                <p>Мы хотим знать о вас немного больше! Учтите, что введеные данные будут публичными</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Заполнение профиля</h3>
                        <p>Вы в любой момент можете изменить ваш профиль: </p>
                    </div>
                    <div class="form-top-right">
                        <i class="fa fa-pencil"></i>
                    </div>
                </div>
                <div class="form-bottom">
                    <form role="form" action="{{route('fillProfile.store')}}" method="post"
                          class="registration-form" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="form-name"><strong><b>Имя и фамилия</b></strong></label>
                            <input type="text" name="name"
                                   required placeholder="Иванов Иван"
                                   class="form-control" id="form-name" value="{{old('name')}}">
                        </div>
                        <div class="form-group">
                            <strong><b>Ваш пол</b></strong>
                            <select name="gender" class="form-control">
                                <option value="1" class="form-control">Мужской</option>
                                <option value="0" class="form-control">Женский</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="form-first-birthday"><strong><b>Дата вашего рождения(День, Месяц,
                                        Год)</b></strong></label>
                            <input type="date" name="birthday" min="1900.01.01" max="<?=date('Y.m.d');?>" required
                                   value="{{old('birthday')}}" class="form-control birthday" id="form-first-birthday">
                        </div>
                        <div class="form-group">
                            <label for="form-country"><strong><b>Страна</b></strong></label>
                            <input type="text" name="country"
                                   required placeholder="Российская Федерация"
                                   class="form-control" id="form-country" value="{{old('country')}}">
                        </div>
                        <div class="form-group">
                            <label for="form-city"><strong><b>Город</b></strong></label>
                            <input type="text" name="city"
                                   required placeholder="Москва"
                                   class="form-control" id="form-city" value="{{old('city')}}">
                        </div>
                        <div class="form-group">
                            <label for="form-status"><strong><b>Статус(не обязательно)</b></strong></label>
                            <textarea name="status"
                                      placeholder="Напишите что-нибудь в своем статусе"
                                      required maxlength="511"
                                      class="form-textarea form-control" id="form-status" rows="10"
                                      cols="10">{{old('status')}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="form-about"><strong><b>Расскажите о себе</b></strong></label>
                            <textarea name="about"
                                      placeholder="Расскажите немного о себе(чем увлекаетесь, о чем мечтаете...)"
                                      required maxlength="10000"
                                      class="form-textarea form-control" id="form-about" rows="10"
                                      cols="10">{{old('about')}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="form-image"><strong><b>Ваша фотография(необязательно)</b></strong></label>
                            <input type="file" name="image"
                                   placeholder="Москва"
                                   class="form-control" id="form-image">
                        </div>
                        <button type="submit" class="btn">Сохранить</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
