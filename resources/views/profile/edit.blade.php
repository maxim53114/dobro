@extends('layouts.main')
@section('content')
    <div class="row">
        <div class="col-sm-12 col-lg-12 col-md-12 col-xl-12 text">
            <h1>Редактирование профиля</h1>
            <div class="description">
                <p>Мы хотим знать о вас немного больше! Учтите, что введеные данные будут публичными</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Изменение профиля</h3>
                        <p>Здесь вы можете изменить ваш профиль: </p>
                    </div>
                    <div class="form-top-right">
                        <i class="fa fa-pencil"></i>
                    </div>
                </div>
                <div class="form-bottom">
                    <form role="form" action="{{route('profile.update')}}" method="post"
                          class="registration-form" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="form-name"><strong><b>Имя и фамилия</b></strong></label>
                            <input type="text" name="name"
                                   required placeholder="Иванов Иван"
                                   class="form-control" id="form-name" value="{{$profile->name}}">
                        </div>
                        <div class="form-group">
                            <strong><b>Ваш пол</b></strong>
                            <select name="gender" class="form-control">
                                @if($profile->gender)
                                    <option value="1" selected class="form-control">Мужской</option>
                                    <option value="0" class="form-control">Женский</option>
                                @else
                                    <option value="1" class="form-control">Мужской</option>
                                    <option value="0" selected class="form-control">Женский</option>
                                @endif
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="form-first-birthday"><strong><b>Дата вашего рождения(День, Месяц,
                                        Год)</b></strong></label>
                            <input type="date" name="birthday" min="1900.01.01" max="<?=date('Y.m.d');?>" required
                                   value="{{$profile->birthday}}" class="form-control birthday" id="form-first-birthday">
                        </div>
                        <div class="form-group">
                            <label for="form-country"><strong><b>Страна</b></strong></label>
                            <input type="text" name="country"
                                   required placeholder="Российская Федерация"
                                   class="form-control" id="form-country" value="{{$profile->country}}">
                        </div>
                        <div class="form-group">
                            <label for="form-city"><strong><b>Город</b></strong></label>
                            <input type="text" name="city"
                                   required placeholder="Москва"
                                   class="form-control" id="form-city" value="{{$profile->city}}">
                        </div>
                        <div class="form-group">
                            <label for="form-status"><strong><b>Статус</b></strong></label>
                            <textarea name="status"
                                      placeholder="Напишите что-нибудь в своем статусе"
                                      required maxlength="511"
                                      class="form-textarea form-control" id="form-status" rows="10"
                                      cols="10">{{$profile->status}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="form-about"><strong><b>Расскажите о себе</b></strong></label>
                            <textarea name="about"
                                      placeholder="Расскажите немного о себе(чем увлекаетесь, о чем мечтаете...)"
                                      required maxlength="10000"
                                      class="form-textarea form-control" id="form-about" rows="10"
                                      cols="10">{{$profile->about}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="form-image"><strong><b>Ваша фотография(оставьте пустым, чтобы оставить прежнее изображение)</b></strong></label>
                            <input type="file" name="image"
                                   placeholder="Москва"
                                   class="form-control" id="form-image">
                        </div>
                        <button type="submit" class="btn">Сохранить</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
