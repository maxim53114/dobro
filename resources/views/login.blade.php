@extends('layouts.main')
@section('content')


    <div class="row">
        <div class="col-sm-12 col-lg-12 col-md-12 col-xl-12 text">
            <h1><strong>Войдите</strong> или <strong>Зарегистрируйтесь</strong></h1>
            <div class="description">
                <p>Зарегистрируйтесь или войдите, если вы уже зарегистрированы</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12  col-md-5 col-lg-5 col-xl-5">

            <div class="form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Войти</h3>
                        <p>Введите email и пароль:</p>
                    </div>
                    <div class="form-top-right">
                        <i class="fa fa-lock"></i>
                    </div>
                </div>
                <div class="form-bottom">
                    <form role="form" action="{{route('login.store')}}" method="post" class="login-form">
                        @csrf
                        <div class="form-group">
                            <label class="sr-only" for="form-username">Email</label>
                            <input type="text" name="email" placeholder="Email"
                                   class="form-username form-control" id="form-username">
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="form-password">Пароль</label>
                            <input type="password" name="password" placeholder="Пароль"
                                   class="form-password form-control" id="form-password">
                        </div>
                        <button type="submit" class="btn">Войти</button>
                    </form>
                </div>
            </div>

            <div class="social-login">
                <div class="social-login-buttons">
                    <a class="btn btn-link-2" href="#">
                        <i class="fa fa-vk"></i> Вход через VK
                    </a>
                </div>
            </div>

        </div>

        <div class="col-sm-1 middle-border"></div>
        <div class="col-sm-1"></div>

        <div class="col-sm-12 col-md-5 col-lg-5 col-xl-5">

            <div class="form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Регистрация</h3>
                        <p>Заполните все поля для регистрации: </p>
                    </div>
                    <div class="form-top-right">
                        <i class="fa fa-pencil"></i>
                    </div>
                </div>
                <div class="form-bottom">
                    <form role="form" action="{{route('register.store')}}" method="post"
                          class="registration-form">
                        @csrf
                        <div class="form-group">
                            <label class="sr-only" for="form-email">Email</label>
                            <input type="text" name="email" placeholder="Email" required
                                   value="{{old('email')}}"
                                   class="form-email form-control" id="form-email">
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="form-password">Пароль</label>
                            <input type="password" name="password" placeholder="Пароль" required
                                   class="form-email form-control" id="form-password">
                        </div>
                        <button type="submit" class="btn">Зарегистрироваться</button>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection
