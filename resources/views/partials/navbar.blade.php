<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{route('main')}}">Главная</a>
            </li>
            @if(Auth::check())
                @if(Auth::user()->status == 'admin')
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.index')}}">Админ-панель</a>
                    </li>
                @endif

                <li class="nav-item">
                    <a class="nav-link" href="{{route('tasks.index')}}">Список задач</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('tasks.my')}}">Мои задачи</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('tasks.active')}}">Активные задачи</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('profile')}}">Профиль</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('logout')}}">Выйти</a>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link" href="{{route('login')}}">Войти</a>
                </li>

            @endif
        </ul>
    </div>
</nav>
