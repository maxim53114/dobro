<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * App\TaskOrder
 *
 * @property-read \App\Task $task
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskOrder newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskOrder newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TaskOrder query()
 * @mixin \Eloquent
 */
class TaskOrder extends Model
{
    const ORDER_IS_INACTIVE = 0;
    const ORDER_IS_ACTIVE = 1;

    protected $table = 'tasks_orders';

    public function task()
    {
        return $this->hasOne('App\Task');
    }

    public static function createNewOrder($task_id, $text)
    {
        $order = new TaskOrder();
        $order->task_id = $task_id;
        $order->executor_id = Auth::id();
        $order->text = $text;

        $order->save();
    }

    /*
     * The task owner cancel executor order
     * Владелец задачи отказал исполнителю, отмена заявки
     * */
    public function cancelForExecutor()
    {
        $this->status = TaskOrder::ORDER_IS_INACTIVE;

        $this->save();
    }

    public function cancelForOwner()
    {
        $this->status = TaskOrder::ORDER_IS_INACTIVE;

        $this->save();
    }

}
