<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Executor
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Executor newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Executor newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Executor query()
 * @mixin \Eloquent
 */
class Executor extends Model
{
    //
}
