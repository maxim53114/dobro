<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\User;
use Illuminate\Http\Request;
use App\Profile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('verified');
    }

    public function index()
    {
        $user = User::find(Auth::id());

        return view('profile.index', [
            'title' => 'Профиль',
            'user' => $user,
        ]);
    }

    public function fill()
    {
        $user = User::findOrFail(Auth::id());
        if($user->status == User::USER_IS_PROFILED) {
            return abort(404, 'Страница не найдена');
        }
        return view('profile.fillProfile', [
            'title' => 'Заполнение профиля'
        ]);
    }

    public function fillStore(ProfileRequest $request)
    {
        $user = User::find(Auth::id());
        if($user->status == User::USER_IS_PROFILED) {
            return abort(404, 'Страница не найдена');
        }

        $profile = Profile::create($request->all());
        $profile->storeImage($request->file('image'));
        User::attachProfileForUser($profile->id);

        return redirect()->route('profile')->with('message', 'Вы успешно заполнили свой профиль');
    }

    public function edit()
    {
        $profile = User::find(Auth::id())->profile;
        return view('profile.edit', [
            'title' => 'Редактировать профиль',
            'profile' => $profile
        ]);
    }

    public function update(ProfileRequest $request)
    {
        $profile = User::find(Auth::id())->profile;
        $profile->deleteImageIfExists($request->file('image'));

        $profile->update($request->all());
        $profile->storeImage($request->file('image'));

        return redirect()->route('profile')->with('message', 'Вы успешно обновили свой профиль');
    }

    public function show($id)
    {
        $user = User::find($id);

        return view('profile.show', [
            'title' => $user->profile->name,
            'user' => $user
        ]);
    }
}
