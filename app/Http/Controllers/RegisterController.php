<?php

namespace App\Http\Controllers;

use App\Mail\VerifiedEmail;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    public function store(RegisterRequest $request)
    {
        $user = User::createNewUser($request);

        if($user) {
            try {
                Mail::to($user->email)->send(new VerifiedEmail($user->verified_key));
            } catch (\Exception $e){
                $user->delete();
                return redirect()->route('login')->with('error', 'Не удалось создать вашу учебную запись, так как вы указали неправильную почту');
            }
            return redirect()->route('login')->with('message', 'Вы успешно зарегистрировались! Можете войти в свой аккаунт. Для активции вашего аккаунты вам необходимо подтвердить дрес электронной почты. Инструкция отправлена на ваш почтовый адрес');
        }
    }
}
