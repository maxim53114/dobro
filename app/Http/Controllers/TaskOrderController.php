<?php

namespace App\Http\Controllers;

use App\Task;
use App\TaskOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskOrderController extends Controller
{
    /*
    * Submit a request(order) to complete the task
    * Исполнитель отправил заявку
    * */
    public function send(Request $request, $task_id)
    {
        $text = $request->get('text');

        if(TaskOrder::where('task_id', $task_id)->where('executor_id', Auth::id())) {
            return redirect()->back()->with('error', 'Вы уже отправили свою заявку, любо вашку заявку отменили');
        }
        if(Task::findOrFail($task_id)->status != Task::TASK_IS_ACTIVE) {
            return redirect()->back()->with('error', 'Задача неактивна, выберите другую');
        }
        if(TaskOrder::createNewOrder($task_id, $text)) {
            return redirect()->back()->with('message', 'Вы успешно отправили заявку, ожидайте отклика');
        } else {
            return redirect()->back()->with('error', 'Произошла ошибка, попробуйте позже');
        }
    }

    /*
     * The task owner cancel executor order
     * Владелец задачи отказал исполнителю, отмена заявки
     * */
    public function cancelForExecutor($id)
    {
        $task = TaskOrder::find($id);
        $task->cancelForExecutor();
    }

    /*
     * The executor cancel your order
     * Исполнитель отменил свою заявку
     * */
    public function cancelForOwner($task_id)
    {
        $order = TaskOrder::where('task_id', $task_id)
            ->where('executor_id', Auth::id())
            ->get();
        if(count($order) < 1) {
            return redirect()->back()->with('error', 'Ошибка');
        }
        $order->cancelForOwner();

    }

}
