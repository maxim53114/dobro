<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Mail\VerifiedEmail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    public function index()
    {
        return view('login', [
            'title' => 'Вход'
        ]);
    }

    public function store(LoginRequest $request)
    {
        if(Auth::attempt($request->only('email', 'password'), true)) {
            if(Auth::user()->status == User::USER_IS_BANNED) {
                Auth::logout();
                return redirect()->back()->with('error', 'Ваш аккаунт заблокирован');
            } else if(Auth::user()->status == User::USER_IS_ACTIVE) {
                @Mail::to(Auth::user()->email)->send(new VerifiedEmail(Auth::user()->verified_key));
                Auth::logout();
                return redirect()->route('main')->with('error', 'Вам необходимо подтвердить свой email, мы повторно отправили вам письмо с инструкцией для подтверждения');
            }
            return redirect()->route('main');
        } else {
            return redirect()->back()->with('error', 'Вы ввели неправильный логин или пароль');
        }
    }

    public function verified($key)
    {
        $user = User::where('verified_key', $key)->firstOrFail();
        if($user->status != 1) {
            return abort(404, 'Страница не найдена');
        }

        $user->status = 2;
        $user->save();

        return redirect()->route('main')->with('message', 'Благодарим вас за подтверждение email, ваша учетная запись активированна');
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('main');
    }
}
