<?php

namespace App\Http\Controllers;

use App\Chat;
use App\Http\Requests\TaskRequest;
use App\Profile;
use App\TaskOrder;
use App\User;
use Illuminate\Http\Request;
use App\Task;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    public function index()
    {
        $tasks = Task::all();

        return view('tasks.index', [
            'title' => 'Список задач',
            'tasks' => $tasks
        ]);
    }

    public function create()
    {
        return view('tasks.create', [
            'title' => 'Создать новую задачу'
        ]);
    }

    public function store(TaskRequest $request)
    {
        $user = User::find(Auth::id());
        if($user->points < $request->get('points')) {
            return redirect()->back()->with('error', 'У вас нет столько баллов');
        }

        Task::createNewTask($request);
        $user->pickUpPoints($request->get('points'));

        return redirect()->route('tasks.index')->with('message', 'Вы успешно создали задачу, ожидайте отклика');
    }

    public function show($id)
    {
        $task = Task::findOrFail($id);
        return view('tasks.show', [
            'title' => $task->description,
            'task' => $task
        ]);
    }

    /*
    * Tasks where this user creator
     * */
    public function my()
    {
        $user = User::find(Auth::id());

        $tasks = $user->tasks;

        return view('tasks.my', [
            'title' => 'Мои задачи',
            'tasks' => $tasks
        ]);
    }

    /*
     * Tasks where this user executor
     * */
    public function active()
    {
        return 123;
    }


    /*
     * The task owner accepted order for executor
     * Владелец задачи выбрал исполнителя
     * */
    public function acceptOrder(Request $request, $task_id, $order_id)
    {
        $order = TaskOrder::find($order_id);
        $task = Task::find($task_id);
        $task->selectExecutor($order->executor_id);
        $task->createChat();

        return redirect()->back()->with('message', 'Вы успешно выбрали исполнителя, теперь вы можете общаться с исполнителем');
    }

    /*
     * Refuse to complete the task
     * Исполнитель отказывается выполнять задачу
     * */
    public function refuseExecutor($task_id)
    {
        $task = Task::find($task_id);
        $task->executorCancelTask();
    }

    /*
     * The requester deletes an issue
     * Заказчик удаляет задачу
     * */

    public function deleteTask($task_id)
    {
        $task = Task::findOrFail($task_id);
        if($task->user_id != Auth::id()) return abort(403, 'У вас нет доступа');

        $task->delete();
    }

    public function delete($task_id)
    {
        $task = Task::findOrFail($task_id);
        if($task->user_id != Auth::id()) {
            return redirect()->back()->with('error', 'Ошибка');
        } else {
            $task->delete();
            $task->orders->delete();
            return redirect()->route('tasks.index')->with('message', 'Вы успешно удалили задачу');
        }
    }

}
