<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    protected $redirectRoute = 'fillProfile';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'birthday' => 'date|required|after:01.01.1900|before:today',
            'name' => 'required',
            'gender' => 'required|boolean',
            'country' => 'required',
            'city' => 'required',
            'status' => 'required|max:511',
            'about' => 'required|max:10000',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ];
    }
}
