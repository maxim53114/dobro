<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class AccessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            $status = Auth::user()->status;
        } else {
            return redirect()->route('login')->with('error', 'Вам необходимо войти или зарегистраваться');
        }
        if($status == User::USER_IS_BANNED) {
            return redirect()->route('main')->with('error', 'Ваш аккаунт заблокирован');
        } else if($status == User::USER_IS_ACTIVE) {
            return redirect()->route('main')->with('error', 'Вам необходимо подтвердить ваш email, инструкция была выслана вам на почту');
        } else if($status == User::USER_EMAIL_IS_VERIFIED) {
            return redirect()->route('fillProfile')->with('error', 'Вам необходимо заполнить ваш профиль');
        } else if($status == User::USER_IS_PROFILED){
            return $next($request);
        } else {
            return redirect()->route('main')->with('error', 'Произошла непредвиденная ошибка, приносим свои извинения');
        }
    }
}
