<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Route;

class VerifiedEmailMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() && Auth::user()->status == User::USER_IS_ACTIVE) {
            if (Route::currentRouteName() == 'main' || Route::current()->uri == '/') {
                \Session::flash('error', 'Для активации вашей учетной записи необходимо перейти по ссылке, которую мы отправили на ваш почтовый адрес');
            } else {
                return redirect()->route('main')->with('error', 'Для активации вашей учетной записи необходимо перейти по ссылке, которую мы отправили на ваш почтовый адрес');
            }
        }
        return $next($request);
    }
}
