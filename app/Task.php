<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * App\Task
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $description
 * @property int $status
 * @property int|null $evaluation
 * @property int|null $points
 * @property string $text
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User|null $author
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereEvaluation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Task whereUserId($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\TaskOrder[] $orders
 * @property-read int|null $orders_count
 */
class Task extends Model
{
    const TASK_IS_ACTIVE = 1;
    const TASK_IS_PROCESSED = 2;
    const TASK_IS_FINISHED = 3;
    const TASK_IS_BAN = 4;

    protected $guarded = ['score'];

    public function author()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function executor()
    {
        return User::find($this->executor_id);
    }

    public function orders()
    {
        return $this->hasMany('App\TaskOrder');
    }

    public static function createNewTask($request)
    {
        $task = Task::create($request->all());
        $task->points = $request->get('points');
        $task->user_id = Auth::id();

        $task->save();
    }

    /*
     * The task owner selected the executor
     * Владелец задачи выбрал исполнителя
     * */
    public function selectExecutor($executor_id)
    {
        $this->executor_id = $executor_id;
        $this->status = Task::TASK_IS_PROCESSED;

        $this->save();
    }


    /*
     * The executor cancel task
     * Исполнитель отказался от выполнения задачи
     * */
    public function executorCancelTask()
    {
        $this->executor_id = null;
        $this->status = Task::TASK_IS_ACTIVE;

        $this->save();
    }

    public function createChat()
    {
        Chat::create([
            'task_id' => $this->id,
            'main_user_id' => $this->user_id,
            'secondary_user_id' => $this->executor_id
        ]);
    }





}
