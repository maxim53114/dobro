<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * App\Profile
 *
 * @property int $id
 * @property string $birthday
 * @property string $name
 * @property string $country
 * @property string $city
 * @property int $gender
 * @property string $status
 * @property string $about
 * @property string|null $image
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereAbout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Profile extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User', 'id', 'profile_id');
    }

    public function getAge()
    {
        $date = $this->birthday;
        $date = explode('-', $date);
        $y = $date[0];
        $m = $date[1];
        $d = $date[2];

        if($m > date('m') || $m == date('m') && $d > date('d')) {
            return (date('Y') - $y - 1);
        } else {
            return (date('Y') - $y);
        }
    }

    public function getGenderText()
    {
        if($this->gender == 1) {
            return 'Мужской';
        } else {
            return 'Женский';
        }
    }

    public function storeImage($image = null)
    {
        if($image) {
            $path = Storage::putFile('public', $image);
            // replacement "public/" to "storage/" in path
            $path = str_replace('public/', '', $path);
            $this->image = $path;

            $this->save();
        }
    }

    public function getImage()
    {
        if($this->image) {
            return '/storage/public/' . $this->image;
        } else {
            return '/storage/public/default.jpeg';
        }
    }

    public function deleteImageIfExists($file)
    {
        if($file) {
            Storage::disk('public')->delete('/public/' . $this->image);
        }
    }

}
