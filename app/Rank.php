<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Rank
 *
 * @property int $id
 * @property string $title
 * @property int $points
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Rank extends Model
{
    //
}
