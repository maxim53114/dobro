<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * App\User
 *
 * @property int $id
 * @property string $email
 * @property string|null $verified_key
 * @property string $password
 * @property int $points
 * @property int $rating
 * @property int $is_verified
 * @property int|null $profile_id
 * @property string|null $role
 * @property int $status
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Profile|null $profile
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Task[] $tasks
 * @property-read int|null $tasks_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIsVerified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereProfileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereVerifiedKey($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;

    const USER_IS_BANNED = 0;
    const USER_IS_ACTIVE = 1;
    const USER_EMAIL_IS_VERIFIED = 2;
    const USER_IS_PROFILED = 3;

    public function profile()
    {
        return $this->belongsTo('App\Profile');
    }

    public function tasks()
    {
        return $this->hasMany('App\Task', 'user_id');
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $redirectTo = '/';


    protected function redirectTo()
    {
        return '/';
    }

    public static function createNewUser($request)
    {
        $user = User::create($request->all());
        $user->password = Hash::make($user->password);
        $user->verified_key = Str::random(40);
        $user->save();

        return $user;
    }

    public static function attachProfileForUser($profileId)
    {
        $user = User::find(Auth::id());
        $user->profile_id = $profileId;
        $user->status = User::USER_IS_PROFILED;
        $user->save();
    }

    public function setProfile($profileId)
    {
        $this->profile_id = $profileId;

        $this->save();
    }

    public function pickUpPoints($points)
    {
        $this->points -= $points;

        $this->save();
    }

    public function setVerifiedStatus()
    {
        $this->status = User::USER_EMAIL_IS_VERIFIED;
        $this->save();
    }

    public function getRank()
    {
        $ranks = Rank::all();
        $profile_rank = $ranks->first()->title;
        foreach ($ranks as $rank) {
            if($rank->score < $this->points) {
                $profile_rank = $rank->title;
            }
        }

        return $profile_rank;
    }
}
