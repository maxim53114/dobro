<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('main');

Route::group(['middleware' => 'guest'], function () {
    Route::get('/login', 'LoginController@index')->name('login');
    Route::post('/register', 'RegisterController@store')->name('register.store');
    Route::post('/login', 'LoginController@store')->name('login.store');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/logout', 'LoginController@logout')->name('logout');
    Route::get('fillProfile', 'ProfileController@fill')->name('fillProfile');
    Route::post('fillProfile', 'ProfileController@fillStore')->name('fillProfile.store');

    Route::group(['middleware' => 'access'], function () {
        Route::get('/profile', 'ProfileController@index')->name('profile');
        Route::get('/profile/edit', 'ProfileController@edit')->name('profile.edit');
        Route::put('/profile/update', 'ProfileController@update')->name('profile.update');
        Route::get('/tasks', 'TaskController@index')->name('tasks.index');
        Route::get('/tasks/create', 'TaskController@create')->name('tasks.create');
        Route::post('/tasks/create', 'TaskController@store')->name('tasks.store');
        Route::get('/my-tasks', 'TaskController@my')->name('tasks.my');
        Route::get('/active-tasks', 'TaskController@active')->name('tasks.active');
        Route::get('/task/{id}', 'TaskController@show')->name('tasks.show');
        Route::get('/id{id}', 'ProfileController@show')->name('profile.show');
    });
});

Route::group(['middleware' => 'admin', 'prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/', 'AdminController@index');
});

Route::get('/verified/{key}', 'LoginController@verified')->name('verified');


