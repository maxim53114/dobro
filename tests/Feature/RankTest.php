<?php

namespace Tests\Feature;

use App\Rank;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RankTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testHasRanks()
    {
        $this->assertTrue(Rank::count() > 0);
    }
}
