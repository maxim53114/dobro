<?php

namespace Tests\Feature;

use App\Http\Middleware\RedirectIfAuthenticated;
use App\Http\Middleware\VerifyCsrfToken;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    public function testRegisterPageFound()
    {
        $response = $this->get(route('login'));
        $response->assertStatus(200);
        $response->assertSeeText('Регистрация');
    }
    public function testFailedAccess()
    {
        $response = $this->get(route('fillProfile'));

        $response->assertRedirect(route('login'));
    }

    public function testFailedValidate()
    {
        $content = [
            'email' => '',
            'password' => ''
        ];
        $this->withoutMiddleware(VerifyCsrfToken::class);
        $response = $this->post(route('register.store'), $content);
        $response->assertRedirect(route('login'));

        $this->assertTrue(User::count() == 0);
    }
    public function testRegister()
    {
        $content = [
            'email' => 'fiks_maksim@mail.ru',
            'password' => 'testPassword'
        ];
        $this->withoutMiddleware(VerifyCsrfToken::class);
        $this->withoutMiddleware(RedirectIfAuthenticated::class);
        $this->assertTrue(User::count() == 0);
        $response = $this->post(route('register.store'), $content);
        $response->assertRedirect(route('login'));

        $this->assertTrue(User::count() == 1);
    }
}
